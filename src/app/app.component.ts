import { Component, OnInit } from '@angular/core';
import { City, CurrentWeather, Daily } from './interfaces/interfaces';
import { LocationService } from './services/location.service';
import { WeatherService } from './services/weather.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  city!: string;
  currentWeather!: CurrentWeather;
  dailys!: Daily[];
  isSearch: boolean = false;
  isLoading: boolean = true;

  constructor(private locationService: LocationService,
              private weatherService: WeatherService) {
  }

  ngOnInit() {
    this.getUserLocation();
  }

  getUserLocation() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition(async (position) => {
        const latitude = position.coords.latitude;
        const longitude = position.coords.longitude;
        await this.getCurrentCity(latitude, longitude);
        await this.getDailyWeathers(latitude, longitude);
        this.isLoading = false;
      }, err => {
        console.log('Ha ocurrido un error al obtener la ubicación.', err);
      }, {
        enableHighAccuracy: true
      });
    }
  }

  async getCurrentCity(latitude: number, longitude: number) {
    try {
      this.city = await this.locationService.getCurrentCity(latitude, longitude);
    } catch (err) {
      console.log('Ha ocurrido un error al obtener la ciudad.', err);
    }
  }

  async getDailyWeathers(latitude: number, longitude: number) {
    try {
      const resp = await this.weatherService.getAllWeathers(latitude, longitude);
      this.currentWeather = resp.current;
      this.dailys = resp.daily;
    } catch (err) {
      console.log('Ha ocurrido un error al obtener la información del clima.', err);
    }
  }

  // Evento que se dispara desde el componente hijo "Search"
  async updateCityAndWeathers(city: City) {
    const { latitude, longitude, name } = city;
    await this.getDailyWeathers(latitude, longitude);
    this.city = name;
    this.isSearch = false;
  }

  onHandleSearch(value: boolean) {
    this.isSearch = value;
  }

}
