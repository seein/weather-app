import { Injectable } from '@angular/core';
import { City } from '../interfaces/interfaces';

@Injectable({
  providedIn: 'root'
})
export class CitiesService {

  cities: Array<City> = [];

  constructor() {
    this.getAll();
  }

  getAll() {
    const cities = JSON.parse(localStorage.getItem('cities')!);
    (cities) ? this.cities = cities : this.cities = [];
  }

  add(city: City) {
    this.cities.push(city);
    localStorage.setItem('cities', JSON.stringify(this.cities));
  }

  delete(city: City) {
    const index = this.cities.findIndex(el => el.name === city.name);
    this.cities.splice(index, 1);
    localStorage.setItem('cities', JSON.stringify(this.cities));
  }

}
