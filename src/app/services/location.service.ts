import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

const BASE_URL_LOCATION = 'https://nominatim.openstreetmap.org';

@Injectable({
  providedIn: 'root'
})
export class LocationService {

  constructor(private http: HttpClient) {
  }

  getCurrentCity(lat: number, lng: number): Promise<string> {
    return this.http.get(`${ BASE_URL_LOCATION }/reverse?format=json&lat=${ lat }&lon=${ lng }`)
      .pipe(
        map((response: any) => response.address.city),
        catchError(err => throwError(err))
      ).toPromise();
  }

  getLatLngCity(city: string) {
    return this.http.get(`${ BASE_URL_LOCATION }/search?format=json&country=Chile&city=${ city }`)
      .pipe(
        map((response: any) => {
          if (response.length === 0) return { 'latitude': 0, 'longitude': 0 }
          return { 'latitude': response[0].lat, 'longitude': response[0].lon }
        }),
        catchError(err => throwError(err))
      ).toPromise();
  }

}
