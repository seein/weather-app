const months = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
const weekdays = ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'];
const today = new Date();

// Devuelve fecha en formato: 'Viernes, 18 de Junio'
export const formatDate = (date: Date) => `${ weekdays[date.getDay()] }, ${ date.getDate() } de ${ months[date.getMonth()] }`;


// Formatea una cadena de texto, en minúsculas y sin acentos
export const formatString = (text: string) => text.normalize('NFD').replace(/[\u0300-\u036f]/g, "").toLocaleLowerCase();

